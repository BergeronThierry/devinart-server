

var socket = io();
(()=>{
    const inputText = document.getElementById('input-text');
    const messages = document.getElementById('messages');
    const button = document.getElementById('button');
    const msgArray = new Array()


    const updateMessages = (message) =>{
        msgArray.push(message);
        
        messages.innerHTML = `${msgArray.map(msg =>{
            return `<p>${msg}</p>`
        })}`
    }

    socket.on('text', (arg) =>{
        updateMessages(arg);
    })



    button.addEventListener('click', ()=>{
        console.log("sending text");
        socket.emit('text', inputText.value);
    })
})()

