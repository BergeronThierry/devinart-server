'use strict';
const uuid = require('uuid');

// ----------------------------------------------------------------------------
// Store room data on server in ram memory
// 
// ----------------------------------------------------------------------------


/**
 * Room =>
 * uuid : string
 * dateCreated : string
 * players : Set? Map? Array?   CLASS object
 * game : currentGame           CLASS object
 * 
 */





class Rooms {
    static instance
    constructor() {
        if (Rooms.instance) {
            return Rooms.instance
        }

        this.rooms = new Map();
        Rooms.instance = this;
    }

    /**
     * Create a room
     */
    createRoom = () => {
        // generate room unique id
        const id = uuid.v4();



        return id;
    }

    getRoom = (uuid) => {

    }

    /**
     * Delete room with id
     * @param {string} roomId 
     */
    deleteRoom = (roomId) =>{
        // 1. CASE => no room error
        if(!this.rooms.has(roomId)) return false;

        // 2. Room deleted
        this.rooms.delete(roomId);

        // 3. True on delete
        return true
    }

    joinRoom = () => {

    }

    leaveRoom = () => {

    }
}

module.exports = new Rooms();