class SessionConnection {
    static instance
    constructor() {
        if (SessionConnection.instance) {
            return SessionConnection.instance;
        }
        this.connections = new Map(); // => <Key: sessionId, Data: {connection: HttpResponse, expiration: millis}>
        this.sessionIds = new Map();  // => <Key: HttpResponse, Data: sessionId>
        SessionConnection.instance = this;
    }

    /**
     * 
     * @param {string} sessionId 
     * @param {HTTPResponse} connection
     */
    addConnection = (sessionId, connection) => {
        // TODO: validation session Id, connection

        // 1. Add connection and expiration to connection map
        const expiration = Date.now() + 3600000;
        this.connections.set(sessionId, {
            connection: connection,
            expiration: expiration 
        });
    }

    /**
     * 
     * @param {HTTPResponse} sessionId 
     */
    getConnection = (sessionId) => {
        return this.connections.get(sessionId);
    }

    /**
     * 
     * @param {HTTPResponse} connection 
     */
    deleteConnectionWithConnection = (connection) => {
        // TODO: implement function
    }

    /**
     * 
     * @param {string} sessionId 
     */
    deleteConnectionWithSessionId = (sessionId) => {
        // TODO: implement function
    }

    /**
     * Purge connection with time
     * 
     */
    purgeConnections = () => {
        // TODO: implement function
    }
}

module.exports = new SessionConnection();