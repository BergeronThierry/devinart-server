
module.exports = async (e) => {
    if(process.env.NODE_ENV !== 'production'){
        let stackTraceArray = e.stack.split("\n")
        let stackLength = stackTraceArray.length
        let stackTrace = stackTraceArray[stackLength-2] + "\n" + stackTraceArray[stackLength-1]

        console.log(`\n-- Error occured --\nMessage : ${e.message}\nStackTrace :\n${stackTrace}\n-- End Error Message --\n`);
    }
}