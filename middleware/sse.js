// ------------------------------------------------------------------------
// Handle live updates
// Connections are stored in roomConnection map, every room has uuid and linked to a set of connection
// 
// ------------------------------------------------------------------------

/**
 * DEV
 */
const consoleLog = require('../functions/consoleLog');


// Session connection
const sessionConnection = require('../state/sessionConnection');

// Identifiant courant des messages
let currentId = 0;

module.exports.connectStream = () => {
    return (req, res, next) => {
        try{
            consoleLog('stream connected');

            const sessionId = 123;
            // Set headers
            res.writeHead(200, {
                'Cache-Control': 'no-cache',
                'Content-Type': 'text/event-stream',
                'Connection': 'keep-alive',
                'X-Accel-Buffering': 'no' // for google app engine nginx
            });
    
            // Add connection to our map
            sessionConnection.addConnection(sessionId, res);
    
            // Keep alive
            const intervalId = setInterval(() => {
                res.write(':\n\n');
                res.flush();
            }, 30000);
    
    
            // Disconnect on client side disconnect
            res.on('close', () => {
                consoleLog('stream disconnected');
                sessionConnection.deleteConnectionWithConnection(res);
                clearInterval(intervalId);
                res.end();
            });
            next();
        }
        catch(err){
            next(err);
        }
    }
}

module.exports.disconnectStreamWithSessionId = (sessionId) => {
    sessionConnection.deleteConnectionWithConnection(sessionId);
}


module.exports.updateStream = (data, eventName, sessionId) => {
    // Bâtir la chaîne de données
    let dataString =
        `id: ${currentId}\n` +
        `data: ${JSON.stringify(data)}\n` +
        (eventName ? `event: ${eventName}\n\n` : '\n');

    // send data linked to sessionId
    const connection = sessionConnection.getConnection(sessionId);
    connection.write(dataString);
    connection.flush();
    currentId++;
}
