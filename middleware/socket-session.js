module.exports = (session) => {
    return (socket, next) =>{
        session(socket.request, {}, next);
    }
}