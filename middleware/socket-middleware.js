module.exports = () =>{
    return (socket, next) =>{
        socket.on('text', (arg) =>{
            socket.emit('text', `serverside : ${arg}`)
            socket.broadcast.emit('text', `serverside : ${arg}`)
        })
        next();
    }
}