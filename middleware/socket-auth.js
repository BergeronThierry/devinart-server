module.exports = () => {
    return (socket, next) =>{
        // Session validation
        if(!socket.session || !socket.session.sessionId){
            const err = new Error("Unauthorized - no sessionId");
            next(err);
        }
        else{
            next();
        }
    }
}