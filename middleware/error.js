const handlErrors = require('../functions/handleErrors');

module.exports = () => {
    return (err, req, res, next) => {

        // Case 403 forbidden
        if(err.httpStatusCode === 403){
            res.status(403).json({
                message: "Unauthorized access"
            })
        }

        if(err.httpStatusCode === 401 && err.httpStatusCode === 400){
            res.send(401).json({
                message: "Invalid request"
            })
        }

        if(err){
            res.send(500).json({
                message: "server - uncaught error"
            })
        }

        /**
         * Log error to console in developement
         */

        if(process.env.NODE_ENV !== 'production'){
            handlErrors(err);
        }
    }
}