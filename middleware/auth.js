module.exports = () => {
    return (req, res, next) => {
        try {
            // implement authentication logic
            const authenticated = req.session.test;
            if (!authenticated) {
                const err = new Error('Unauthorized');
                err.httpStatusCode = 403;
                next(err);
            }
            else {
                // continue to next middleware
                next();
            }
        }
        catch (err) {
            err.httpStatusCode = 401
            next(err);
        }
    }
}