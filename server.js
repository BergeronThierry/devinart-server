/**
 * IMPORTS
 */
const app = require('express')();
const session = require('express-session');
const MemoryStore = require('memorystore')(session);
const helmet = require('helmet');
const compression = require('compression');
const cors = require('cors');
const serveStatic = require('serve-static');
const bodyParser = require('body-parser');
const errorHandler = require('./middleware/error');
const auth = require('./middleware/auth');
const socketSession = require('./middleware/socket-session');
const socketMiddleware = require('./middleware/socket-middleware');

// Commentaire TEST
// Commentaire 2
// Commentaire 3

/**
 * WEBSOCKETS
 */
const http = require('http').Server(app);
const io = require('socket.io')(http);

/**
 * ENVIRONMENT VARIABLES
 */
const PORT = process.env.PORT || 3003;
const SESSION_SECRET = process.env.SESSION_SECRET || 'A_very_secret_secret';
const NODE_ENV = process.env.NODE_ENV || 'developpement';


/**
 * HTTP -- MIDDLEWARES
 */
app.set('trust proxy', true) // For google app engine flex environment
app.use(compression());
//app.use(helmet());
//app.use(cors());
app.use(bodyParser.json({ strict: false }));
const sessionMiddleware = session({
    cookie: { maxAge: 3600000 },
    name: process.env.npm_package_name,
    store: new MemoryStore({ checkPeriod: 3600000 }),
    resave: false,
    saveUninitialized: false,
    secret: SESSION_SECRET
})
app.use(sessionMiddleware);
app.use(serveStatic('./test-client'));


/**
 * WEBSOCKET -- MIDDLEWARES
 */
io.use(socketSession(sessionMiddleware));
io.use(socketMiddleware());
io.on('connection', (socket) =>{
    socket.on('disconnect', ()=>{
        console.log('websocket disconnected');
    })
})


/**
 * HTTP -- ROUTES
 */
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/test-client/index.html');
})
app.delete('/', (req, res) => {
    req.session.destroy();
    res.status(200).json({
        message: "logged out"
    })
})
app.get('/auth', auth(), (req, res) => {
    res.status(200).json({
        message: "Access authorized"
    })
})



/**
 * END OF PIPELINE
 */
app.use((err, req, res, next) => {
    if (!err) {
        res.status("404").json({
            message: "Not Found"
        })
    }
    else {
        next(err);
    }
})
app.use(errorHandler());



http.listen(PORT, () => {
    console.log(`listening on port ${PORT}`);
});

//http.listen()